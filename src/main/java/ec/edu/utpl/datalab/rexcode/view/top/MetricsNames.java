/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.rexcode.view.top;

import ec.edu.utpl.datalab.rexcode.metrics.base.Plataforma;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.components.JParcer;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.HalsteadVisitor;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaCLocClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaCycloClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaDCClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaHalsteadBugsClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaHalsteadDifficultClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaHalsteadEfforClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaHalsteadVocabularyClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaHalsteadVolumenClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.clase.FormulaLocClass;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.file.FormulaCycloFile;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.file.FormulaLocFile;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.file.FormulaMantenibilityIndexFile;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.fragment.FormulaAverageCycloFragment;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.fragment.FormulaCLocFragment;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.fragment.FormulaDCFragment;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.fragment.FormulaLocFragment;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.fragment.FormulaNumMethodFragment;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.fragment.FormulaTimeHalsteadFragment;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaAvgMantenibilityIndexProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaCLocProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaDCProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaLocProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaNumClassProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaNumMethodProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaNumeroFicherosProject;
import ec.edu.utpl.datalab.rexcode.metrics.base.java.metrics.formulas.project.FormulaTimeHalsteadProject;
import java.util.Map;
import java.util.TreeMap;


public class MetricsNames {
    public static final Plataforma plataforma = new Plataforma();
    public static final Map<String, String> packageMetrics = new TreeMap<String, String>();
    public static final Map<String, String> class1Metrics = new TreeMap<String, String>();
    public static final Map<String, String> class2Metrics = new TreeMap<String, String>();
    public static final Map<String, String> methodMetrics = new TreeMap<String, String>();
    public static final Map<String, String> commonMetrics = new TreeMap<String, String>();
    
    static {
        plataforma.findAndInstallModules();
        
        
        packageMetrics.put("A", "Abstractness");
        packageMetrics.put("AC", "Afferent Coupling");
        packageMetrics.put("D", "Distance");
        packageMetrics.put("EC", "Efferent Coupling");
        packageMetrics.put("I", "Instability");
        packageMetrics.put("NCP", "Number of Classes in Package");
        packageMetrics.put("NIP", "Number of Interfaces in Package");
        packageMetrics.put("C", "Coverage");

        class1Metrics.put("LCC", "Loose Class Coupling");
        class1Metrics.put("LCOM1", "Lack of COhesion in Methods 1");
        class1Metrics.put("LCOM2", "Lack of COhesion in Methods 2");
        class1Metrics.put("LCOM3", "Lack of COhesion in Methods 3");
        class1Metrics.put("LCOM4", "Lack of COhesion in Methods 4");
        class1Metrics.put("LCOM5", "Lack of COhesion in Methods 5");
        class1Metrics.put("NAK", "Number of Assertions per KLOC");
        class2Metrics.put("NOC", "Number Of Children");
        class2Metrics.put("NOF", "Number Of Fields");
        class2Metrics.put("NOM", "Number Of Methods");
        class2Metrics.put("NOSF", "Number Of Static Fields");
        class2Metrics.put("NOSM", "Number Of Static Methods");
        class2Metrics.put("NTM", "Number of Test Methods");
        class2Metrics.put("TCC", "Tight Class Coupling");
        class2Metrics.put("WMC", "Weighted Method Count");

        methodMetrics.put("NBD", "Nested Block Depth");
        methodMetrics.put("NOP", "Number Of Parameters");
        methodMetrics.put("VG", "McCabe's Cyclomatic Complexity");
        
        commonMetrics.put("LOC", "Lines Of Code");
        commonMetrics.put("LOCm", "Lines Of Comments");
    }
}
